# Catalyst Support
This is a privately developed and maintained package, providing various auxiliary features to the Catalys modelling package (https://github.com/SciML/Catalyst.jl).

Currently it contains:

### An Expanded "Model" Structure"
A structure, `Model`, which contains a `ReactionSystem`, a parameter set, and some auxiliary information.

### Simulation Quick Commands
A couple of functions; `detsim()`, `stochsim()`, `monte()`, `ssasim()`, and `ssamonte()`, which bundles problem creation and solving.

### A couple of Callbacks
For creating steps in paraemter or varriable values. Also a simple `positive_domain()` callback.

### Steady State Function
A function, `steady_states()`, which calls the require homotopy continuation functionality for finding the steady states of a model.