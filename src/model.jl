### The Model Structure ###

# A structure to contain all the information of a biochemical reaction network model.
mutable struct Model
    system::ReactionSystem
    p_vals::Vector{Float64}
    original_p_vals::Vector{Float64}
    v_syms::Vector{Symbol}
    p_syms::Vector{Symbol}
    η::Union{ModelingToolkit.Num,Nothing}
    constraints::Vector{Any}
    hc_template::Union{NamedTuple,Nothing}

    function Model(system,p;constraints=[],hc_template=nothing)
        if length(p) == length(system.ps) + 1
            new(system,copy(p),copy(p),map(i -> Symbol(system.states[i].val.f.name), 1:length(system.states)),vcat(map(i -> Symbol(system.ps[i]), 1:length(system.ps)),:η),(ModelingToolkit.@parameters η)[1],constraints,hc_template)
        elseif in(:η,Symbol.(system.ps))
            new(system,copy(p),copy(p),map(i -> Symbol(system.states[i].val.f.name), 1:length(system.states)),map(i -> Symbol(system.ps[i]), 1:length(system.ps)),(ModelingToolkit.@parameters η)[1],constraints,hc_template)
        else
            new(system,copy(p),copy(p),map(i -> Symbol(system.states[i].val.f.name), 1:length(system.states)),map(i -> Symbol(system.ps[i]), 1:length(system.ps)),nothing,constraints,hc_template)
        end
    end
end

### Auxiliary Functions ###

# Resets the parameter values of a model to their original values.
function reset!(model::Model)
    model.p_vals = copy(model.original_p_vals)
end

# Uses symbols as indexing.
function getindex(model::Model,sym::Symbol)
    !in(sym,model.p_syms) && error("The model have no parameter $sym.")
    return getindex(model.p_vals,findfirst(sym.==model.p_syms))
end
function setindex!(model::Model,val,sym::Symbol)
    !in(sym,model.p_syms) && error("The model have no parameter $sym.")
    return setindex!(model.p_vals,val,findfirst(sym.==model.p_syms))
end
