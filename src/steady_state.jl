### Finds Steady States ###

# Finds the steadys states of a biochemical reaction network model.
steady_states(system::ReactionSystem,p_vals::Vector{Float64},args...;constraints=[],hc_template_eqs=nothing,kwargs...) = steady_states(Model(system,p_vals,args...;constraints=constraints,hc_template_eqs=hc_template_eqs,kwargs...))
function steady_states(model::Model)
    (model.hc_template==nothing) && (make_hc_template!(model))
    p_vals = (model.η==nothing) ? model.p_vals : model.p_vals[1:end-1]
    hc_eqs = HCMK.subs(deepcopy(model.hc_template.hc_eqs),Dict([model.hc_template.hc_params[i]=>(p_val%1.0==0.0) ? Int64(p_val) : p_val for (i,p_val) in enumerate(p_vals)]))
    hc_eqs = map(eq -> clean_denominator(eq), hc_eqs)
    hc_sys = HC.System(hc_eqs; variables = model.hc_template.hc_vars, parameters = model.hc_template.hc_params)

    sol = HC.solve(hc_sys, show_progress=false)
    return filter(s->!any(s.<0),real_solutions(sol))
end

# Generates template hc equations, which can be solved (after parameter exchange and denominator removal). This part takes time even for small systems.
make_hc_template!(model::Model) = (model.hc_template = make_hc_template(model.system))
make_hc_template(model::Model) = make_hc_template(model.system)
function make_hc_template(system)
    ns = convert(NonlinearSystem,system)
    hc_vars = HC.Variable.(Symbol.(ns.states))
    hc_params = HC.Variable.(Symbol.(ns.ps))
    nlsys_func = generate_function(ns, map(var -> Operation(var,Vector{MTK.Expression}()), ns.states), map(var -> Operation(var,Vector{MTK.Expression}()), ns.ps), expression=Val{false})[1]
    return (hc_params=hc_params,hc_vars=hc_vars,hc_eqs=nlsys_func(hc_vars, hc_params))
end


### Extends HomotopyContinuation (Provided by Sascha Timme) ###

# Allows HomotopyContinuation to work on rational polynomials.
function clean_denominator(expr)
    cls = HCMK.class(expr)
    if cls == :Add
        # collect denominators
        denom = HCMK.Expression(1)
        for arg in HCMK.args(expr)
            if HCMK.class(arg) == :Mul
                mul_args = HCMK.args(arg)
                for mul_arg in mul_args
                    if HCMK.class(mul_arg) == :Pow
                        base, exp = HCMK.args(mul_arg)
                        if HCMK.to_number(exp) < 0
                            denom *= HCMK.Expression(1) / mul_arg
                        end
                    end
                end
            end
        end
        sum(arg -> arg * denom, HCMK.args(expr))
    elseif cls == :Mul
        prod(arg -> clean_denominator(arg), HCMK.args(expr))
    else
        expr
    end
end

# This allows the replacements of exponent parameters in HomotopyContinuation.
function Base.:(^)(x::HCMK.Basic,n::HCMK.Variable)
    a = HCMK.Expression()
    ccall(
        (:basic_pow, HCMK.libsymengine),
        Nothing,
        (Ref{HCMK.Expression}, Ref{HCMK.ExpressionRef}, Ref{HCMK.Expression}),
        a,
        x,
        HCMK.Expression(n),
    )
    return a
end
