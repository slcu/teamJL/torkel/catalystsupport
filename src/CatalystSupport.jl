module CatalystSupport

# Make Imports.
using DiffEqJump, OrdinaryDiffEq, StochasticDiffEq
using Catalyst
#using HomotopyContinuation

import Base.getindex, Base.setindex!
import ModelingToolkit.Num
#import HomotopyContinuation.ModelKit.args, HomotopyContinuation.ModelKit.class

# Sets constants.
const MTK = ModelingToolkit
#const HC = HomotopyContinuation
#const HCMK = HomotopyContinuation.ModelKit

# Include files.
include("model.jl")
include("simulate.jl")
#include("steady_state.jl")

# Make exports.
export Model, reset!, getindex, setindex!
export detsim, stochsim, monte, ssasim, ssamonte
export steady_states, make_hc_template

end # module
