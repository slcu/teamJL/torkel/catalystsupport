### ODE Simulation ###

# Makes a deterministic simulation.
detsim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = detsim(Model(system,p_vals),args...;kwargs...)
function detsim(model::Model, tspan; u0=det_u0(model,tspan[2]/10.), solver=Rosenbrock23(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = ODEProblem(model.system,u0,tspan,deepcopy((model.η==nothing) ? model.p_vals : model.p_vals[1:end-1]))
    return OrdinaryDiffEq.solve(prob,solver;callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps),kwargs...)
end

### SDE Simulation  ###

# Makes a stochastic simulation.
stochsim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = stochsim(Model(system,p_vals),args...;kwargs...)
function stochsim(model, tspan; dt = 0.001, adaptive = true, u0=sde_u0(model,tspan[2]/10.,dt,adaptive), solver=ImplicitEM(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = SDEProblem(model.system,u0,tspan,deepcopy(model.p_vals),noise_scaling=model.η)
    return StochasticDiffEq.solve(prob,solver;callback=CallbackSet(positive_domain(),par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), dt=dt, adaptive=adaptive, kwargs...)
end

# Makes several stochastic simulations.
monte(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = monte(Model(system,p_vals),args...;kwargs...)
function monte(model, tspan, n; dt = 0.001, adaptive = true,  ss_nbr=1, u0s=sde_u0s(n,model,ceil(Int64,n/5),tspan[2]/10,dt,adaptive), solver=ImplicitEM(), eSolver=EnsembleThreads(), p_steps=(), v_steps=(), callbacks=(), kwargs...)
    prob = SDEProblem(model.system,u0s[1],tspan,deepcopy(model.p_vals),noise_scaling=model.η)
    ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->remake(p;u0=u0s[i]))
    return StochasticDiffEq.solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(positive_domain(),par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), dt=dt, adaptive=adaptive, kwargs...)
end

### SSA Simulation ###

ssasim(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = ssasim(Model(system,p_vals),args...;kwargs...)
# With p_steps or v_steps.
function ssasim(model, tspan; u0=ssa_u0(model,tspan[2]/10.),solver=SSAStepper(), callbacks=(), p_steps=(), v_steps=(), kwargs...)
    dprob = DiscreteProblem(model.system, u0, tspan, model.p_vals)
    jprob = JumpProblem(model.system, dprob, Direct(), save_positions=(false,false))    
    return DiffEqJump.solve(jprob,solver;callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), kwargs...)
end

ssamonte(system::ReactionSystem,p_vals::Vector{Float64},args...;kwargs...) = ssamonte(Model(system,p_vals),args...;kwargs...)
function ssamonte(model, tspan, n; u0s=ssa_u0s(n,model,ceil(Int64,n/5),tspan[2]/10), eSolver=EnsembleThreads(), solver=SSAStepper(), callbacks=(), p_steps=(), v_steps=(), kwargs...)
    dprob = DiscreteProblem(model.system, u0s[1], tspan, model.p_vals)
    jprob = JumpProblem(model.system, dprob, Direct(),save_positions=(false,false))    
    ensemble_prob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p;p=deepcopy(model.p_vals),u0=u0s[i]),safetycopy=false)
    return DiffEqJump.solve(ensemble_prob,solver,eSolver;trajectories=n,callback=CallbackSet(par_steps(p_steps,model.p_syms)...,var_steps(v_steps,model.v_syms)...,callbacks...),tstops=find_tstops(p_steps,v_steps), kwargs...)
end

### Initial Conditions Deciders ###

# Simualtes a deterministic model until it reaches a fp.
function det_u0(model,l)
    sol = detsim(model,(0.,l);u0=fill(1.,length(model.v_syms)))
    return sol.u[end]
end

# Simualtes a SDE model, and makes a selction of values at the fp distribution
sde_u0(model,l,dt,adaptive) = sde_u0s(1,model,1,l,dt,adaptive)[1]
function sde_u0s(N,model,n,l,dt,adaptive)
    sols = monte(model,(0.,l),n;u0s=fill(fill(1.,length(model.v_syms)),n),saveat=l/100., dt=dt, adaptive=adaptive)
    return map(i -> sols[rand(1:n)].u[rand(20:end)], 1:N)
end

# Simualtes a SSA model, and makes a selction of values at the fp distribution
ssa_u0(model,l) = ssa_u0s(1,model,1,l)[1]
function ssa_u0s(N,model,n,l)
    sols = ssamonte(model,(0.,l),n;u0s=fill(fill(1,length(model.v_syms)),n),saveat=l/100.)
    return map(i -> sols[rand(1:n)].u[rand(20:end)], 1:N)
end


### Steady State Interfaces ###

#Find initial conditions (for continious simulation).
function makeu0(model, starting_steady_state_number=1) # Need to handle fixed concentrations...
    ss = steady_states(model)
    if (length(ss)==0)
        @warn "No initial steady state found."
        ss = ones(length(model.syms))
    end
    return sort(ss)[starting_steady_state_number]
end

#Find initial conditions (for discrete simulation).
function ssa_equi(model; fixed_conc...) # Need to handle fixed concentrations somehow.
    uInit = fill(10, length(model.system.states))
    prob = JumpProblem(model.system,DiscreteProblem(model.system,uInit,(0,100.),model.p_vals),Direct())
    sol = DiffEqJump.solve(prob,SSAStepper());
    return sol[end]
end


### Callbacks ###

#A callback for making a step increase in a single parameter.
function par_step(p_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.p[p_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several parameters.
function par_steps(p_steps,p_syms)
    output = Vector{DiscreteCallback}()
    for steps in my_split(p_steps), step in steps[2:end]
        push!(output,par_step(findfirst(p_syms.==steps[1]),step...))
    end
    return output
end

#A callback for making a step increase in a single variable.
function var_step(v_idx,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.u[v_idx] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several variables.
function var_steps(v_steps,v_syms)
    output = Vector{DiscreteCallback}()
    for steps in my_split(v_steps), step in steps[2:end]
        push!(output,var_step(findfirst(v_syms.==steps[1]),step...))
    end
    return output
end

#A callback for keeping a simulation within the positive domain.
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end


### Callbacks Auxillary Functions ###

#Splits an array acording to my desires.
function my_split(array)
    starts = findall(typeof.(array).==Symbol)
    ends = [starts[2:end]...,length(array)+1].-1
    return [array[starts[i]:ends[i]] for i in 1:length(starts)]
end

#Finds the tstops for a given callback vector.
function find_tstops(p_steps,v_steps)
    first.(filter(i->typeof(i)!=Symbol,[p_steps...,v_steps...]))
end
